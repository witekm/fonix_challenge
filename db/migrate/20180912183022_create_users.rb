class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :phone, null: false
      t.string :confirmation_token
      t.boolean :confirmed, default: false

      t.timestamps
    end
  end
end
