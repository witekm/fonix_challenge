Rails.application.routes.draw do
  get 'panel/home'
  root 'registrations#show'
  resource :registrations, only: [:show, :create]
  resource :confirmations, only: [:show, :create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
