FactoryBot.define do
  factory :user do
    phone { "MyString" }
    confirmation_token { "MyString" }
    confirmed { false }
  end
end
