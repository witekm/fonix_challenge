require 'rails_helper'

RSpec.describe RegistrationsController, type: :controller do

  describe "GET #show" do
    it "returns http success" do
      get :show
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #create" do
    subject       { post :create, params: params }
    before(:each) { VCR.use_cassette('send_sms') do
                      subject
                    end
                  }

    context 'with proper data' do
      let!(:params) { { user: {
          phone: "48515070656" }
      } }

      it "returns http success" do
        expect(response).to have_http_status(:redirect)
      end

      it 'redirects to confirmations path' do
        expect(response).to redirect_to(confirmations_path(phone: params[:user][:phone]))
      end

      it 'sets user confirmation to false' do
        user = User.find_by_phone(params[:user][:phone])
        expect(user.confirmed).to eq false
      end
    end

    context 'with wrong data' do
      let!(:params) { { user: {
          phone: "+485018" }
      } }

      it "returns http redirect" do
        expect(response).to have_http_status(:redirect)
      end

      it 'redirects to registrations path' do
        expect(response).to redirect_to(registrations_path)
      end

      it 'sets alert message' do
        expect(flash[:alert]).to match /Phone must be parseable as Phone number/
      end
    end
  end

end
