require 'rails_helper'

RSpec.describe ConfirmationsController, type: :controller do

  describe "GET #show" do
    context 'with phone passed' do
      subject { get :show, params: { phone: '+48880998778' }}

      it 'returns http success' do
        subject
        expect(response).to have_http_status(:success)
      end

      it 'renders show action' do
        expect(subject).to render_template(:show)
      end
    end

    context 'without phone in params' do
      subject       { get :show }
      before(:each) { subject }

      it 'returns http redirect' do
        expect(response).to have_http_status(:redirect)
      end

      it 'redirects to registration path' do
        expect(response).to redirect_to registrations_path
      end
    end
  end

  describe "POST #create" do
    let(:confirmation_params) { {confirmation: {
                                                phone: '+48566766866',
                                                token: '1234'
                              }} }

    context 'when token match' do
      subject { post :create, params: confirmation_params }
      before(:each) do
        User.create(phone: confirmation_params[:confirmation][:phone], confirmation_token: confirmation_params[:confirmation][:token])
        subject
      end

      it 'returns http redirect' do
        expect(response).to have_http_status(:redirect)
      end

      it 'redirects to panel home' do
        expect(response).to redirect_to panel_home_path
      end

      it 'marks user as confirmed' do
        expect(User.find_by(phone: confirmation_params[:confirmation][:phone]).confirmed?).to eq true
      end
    end

    context 'when token does not match' do
      subject { post :create, params: confirmation_params }
      before(:each) do
        User.create(phone: confirmation_params[:confirmation][:phone], confirmation_token: '0000')
        subject
      end

      it 'returns http redirect' do
        expect(response).to have_http_status(:redirect)
      end

      it 'redirects to registrations path' do
        expect(response).to redirect_to registrations_path
      end

      it 'has flash message - Phone or token ' do
        expect(flash[:alert]).to match /Phone number and\/or token do not match!/
      end
    end

    context 'when phone number is wrong' do
      subject { post :create, params: confirmation_params }
      before(:each) do
        User.create(phone: '+44433', confirmation_token: confirmation_params[:confirmation][:token])
        subject
      end

      it 'returns http redirect' do
        expect(response).to have_http_status(:redirect)
      end

      it 'redirects to registrations path' do
        expect(response).to redirect_to registrations_path
      end

      it 'has flash message - Phone or token' do
        expect(flash[:alert]).to match /Phone number and\/or token do not match!/
      end
    end
  end

end
