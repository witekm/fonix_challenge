require 'rails_helper'

RSpec.describe UserConfirmationService do
  let(:confirmation_params) { { phone: '48515070656', token: '1234' } }

  subject { described_class.new(confirmation_params) }

  context 'when initializing' do

    it 'has proper phone assigned' do
      expect(subject.phone).to eq confirmation_params[:phone]
    end

    it 'has proper token assigned' do
      expect(subject.token).to eq confirmation_params[:token]
    end
  end

  context 'when token not yet confirmed' do
    before do
      User.create(phone: confirmation_params[:phone], confirmation_token: confirmation_params[:token])
    end

    it 'confirms user token' do
      subject.call
      expect(User.find_by(phone: confirmation_params[:phone]).confirmed?).to be true
    end

    it 'returns Result::Success' do
      expect(subject.call).to be_kind_of(Result::Success)
    end

    it 'provides success message with result' do
      result = subject.call
      expect(result.notice).to match /Access granted!/
    end
  end

  context 'when token already confirmed' do
    before do
      User.create(phone: confirmation_params[:phone], confirmation_token: confirmation_params[:token], confirmed: true)
    end

    it 'returns Result::Error' do
      expect(subject.call).to be_kind_of(Result::Error)
    end

    it 'provides error message with result' do
      result = subject.call
      expect(result.error).to match /Please, generate new token!/
    end
  end

  context 'when phone is unknown' do
    it 'returns Result::Error' do
      expect(subject.call).to be_kind_of(Result::Error)
    end

    it 'provides error message with result' do
      result = subject.call
      expect(result.error).to match /Phone number and\/or token do not match!/
    end
  end
end