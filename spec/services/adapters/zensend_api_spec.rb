require 'rails_helper'

RSpec.describe Zensend::Api do

  subject { described_class.new }

  context 'when initializing' do

    it 'has api_key assigned' do
      expect(subject.send(:api_key)).to be_present
    end
  end

  describe '#send_sms' do

    context 'when all needed data is passsed' do
      it 'returns true' do
        VCR.use_cassette('send_sms') do
          expect(subject.send_sms('48515000111', '1234')).to eq true
        end
      end
    end

    context 'when phone number is missing' do
      it 'returns false' do
        VCR.use_cassette('send_sms_when_phone_missing') do
          expect(subject.send_sms('', '1234')).to eq false
        end
      end
    end
  end
end