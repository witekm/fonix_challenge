require 'rails_helper'

RSpec.describe UserRegistrationService do
  let(:service_params) { { phone: '48515070656' } }
  let(:registration_form) { RegistrationForm.new(service_params) }

  subject { described_class.new(registration_form) }

  context 'when initializing' do

    it 'has proper phone assigned' do
      expect(subject.phone).to eq registration_form.phone
    end

    it 'has confirmation_token longer than 3 digits' do
      expect(subject.token.length).to be > 3
    end
  end

  context 'when performing action' do

    it 'creates user' do
      VCR.use_cassette('send_sms') do
        subject.call
        expect(User.find_by(phone: service_params[:phone])).to be_present
      end
    end

    it 'sends sms' do
      expect(subject).to receive(:send_sms).and_return(true)
      subject.call
    end

    it 'returns result success' do
      VCR.use_cassette('send_sms') do
        expect(subject.call).to be_kind_of(Result::Success)
      end
    end
  end
end