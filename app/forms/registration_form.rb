class RegistrationForm
  include Virtus.model
  include ActiveModel::Validations

  attribute :phone, String

  validates_presence_of :phone

  validates_each :phone do |record, attr, value|
    record.errors.add(attr, 'must be parseable as Phone number') unless Phoner::Phone.valid?("+#{value}")
  end

end