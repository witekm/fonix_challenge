class UserConfirmationService
  class << self
    def call(params)
      self.new(params).call()
    end
  end

  attr_reader :phone, :token

  def initialize(params)
    @phone = params[:phone]
    @token = params[:token]
  end

  def call
    user = User.find_by!(phone: phone)

    unless user.confirmed?
      if user.confirmation_token == token
        user.update(confirmed: true)
        return Result::Success.new(user, 'Access granted!')
      else
        return Result::Error.new('Phone number and/or token do not match!')
      end
    end

    Result::Error.new('Please, generate new token!')
  rescue ActiveRecord::RecordNotFound
    Result::Error.new('Phone number and/or token do not match!')
  end

  private


end