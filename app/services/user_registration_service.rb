class UserRegistrationService
  class << self
    def call(form)
      self.new(form).call()
    end
  end

  attr_reader :phone, :token

  def initialize(form)
    @phone = form.phone
    @token = random_token
    @sender_class = Zensend::Api
  end

  def call
    user = User.find_or_create_by!(phone: phone)
    user.update(confirmation_token: token, confirmed: false)
    send_sms and return Result::Success.new(user, 'User registered!') if user.valid?

    Result::Error.new('Something went wrong!')
  end

  private
  attr_reader :sender_class

  def random_token
    token = 0
    while token.to_s.length < 4 do
      token = SecureRandom.random_number(100000)
    end
    token.to_s
  end

  def send_sms
    sender = sender_class.new
    sender.send_sms(phone, token)
  end
end