module Zensend
  class Api
    include HTTParty
    base_uri 'api.zensend.io:443'

    def initialize
      @api_key = Rails.application.credentials.zensend[:api_key]
      @originator = '48515070656'
      @options = { headers: {
                              "X-API-KEY": api_key
                            }}
    end

    def send_sms(phone, content)
      response = self.class.post("/v3/sendsms", options.merge(
                                 body: { BODY: content,
                                         ORIGINATOR: originator,
                                         NUMBERS: phone}))

      response.code == 200 ? true : false
    rescue HTTParty::Error, StandardError
      false
    end

    def test
      response = self.class.get("/v3/checkbalance", options)
      true
    rescue HTTParty::Error, StandardError
      false
    end

    private
    attr_reader :api_key, :options, :originator
  end
end