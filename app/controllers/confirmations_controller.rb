class ConfirmationsController < ApplicationController
  def show
    phone = params[:phone]

    if phone.present?
      render "show", locals: { phone: phone } and return
    else
      redirect_to registrations_path
    end
  end

  def create
    result = UserConfirmationService.(confirmation_params)
    result.success? ? render_service_success(result, panel_home_path) : render_service_error(result, registrations_path)
  end

  private

  def confirmation_params
    params.require(:confirmation).permit(:phone, :token)
  end
end
