module RenderHelper
  extend ActiveSupport::Concern

  def render_form_validation_fail form, address_to_redirect
    redirect_to address_to_redirect, alert: form.errors.full_messages.join(', ')
  end

  def render_service_error result, path
    flash[:alert] = result.error
    redirect_to path
  end

  def render_service_success result, path
    flash[:notice] = result.notice
    redirect_to path
  end
end