class RegistrationsController < ApplicationController
  def show
  end

  def create
    registration_form = RegistrationForm.new(user_params)
    registration_form.valid? or ( render_form_validation_fail(registration_form, registrations_path) and return )

    result = UserRegistrationService.(registration_form)

    result.success? ? redirect_to(confirmations_path(phone: registration_form.phone)) \
                      : \
                      redirect_to(registrations_path, alert: result.error)
  end

  private

  def user_params
    params.require(:user).permit(:phone)
  end
end
